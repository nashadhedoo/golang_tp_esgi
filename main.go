package main

import (
	"bufio"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
	"time"
)

func getTime(w http.ResponseWriter, req *http.Request) {
	time := time.Now()
	switch req.Method {
	case http.MethodGet:
		fmt.Fprintf(w, "Heure : %v%v%v", time.Hour(), "h", time.Minute())
	}
}

func addEntry(w http.ResponseWriter, req *http.Request) {
	switch req.Method {
	case http.MethodPost:
		if err := req.ParseForm(); err != nil {
			fmt.Println("Something went bad")
			fmt.Fprintln(w, "Something went bad")
			return
		}
		newEntry := make(map[string]string)
		fmt.Fprintf(w, "Information received: %v\n", req.PostForm)
		for key, value := range req.PostForm {
			if key == "entry" {
				newEntry["entry"] = fmt.Sprint(value[0])
			}
			if key == "author" {
				newEntry["author"] = fmt.Sprint(value[0])
			}
		}
		saveEntry(newEntry)
	}
}

func saveEntry(newEntry map[string]string) {
	saveFile, err := os.OpenFile("./entry.txt", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0755)
	defer saveFile.Close()
	w := bufio.NewWriter(saveFile)
	if err == nil {
		fmt.Fprintf(w, "%s: %s\n", newEntry["author"], newEntry["entry"])
	}
	w.Flush()
}

func getEntry(w http.ResponseWriter, req *http.Request) {
	switch req.Method {
	case http.MethodGet:

		file, err := os.Open("entry.txt")
		if err != nil {
			log.Fatal(err)
		}
		defer file.Close()
		sc := bufio.NewScanner(file)
		for sc.Scan() {
			if len(strings.Split(sc.Text(), ":")) > 1 {
				fmt.Fprintf(w, "%s\n", strings.Split(sc.Text(), ":")[1])
			}
		}
		if err := sc.Err(); err != nil {
			log.Fatal(err)
		}

	}
}

func main() {
	http.HandleFunc("/time", getTime)
	http.HandleFunc("/entry", getEntry)
	http.HandleFunc("/add", addEntry)
	http.ListenAndServe(":4567", nil)
}